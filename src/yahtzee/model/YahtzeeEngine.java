package yahtzee.model;

import java.util.Vector;

/**
 * This is the yahtzee engine for the yahtzee game. This class
 * sets up the conditions of the game.
 * 
 * @author Michael Chen
 * @author Mikhail Maksimov
 */
public class YahtzeeEngine
{
    public static final int MAX_ROLLS = 3;

    private Vector<Player> myPlayers;
    private Player myPlayerUp;
    private Roller myRoller;
    private int myNumberRollsUsed;
    private boolean myGameFinished;
    
    
    /**
     * Default Yahtzee engine constructor.
     * 
     * Takes number of players and add players in vector.  
     */
    public YahtzeeEngine(int numPlayers)
    {
    	if (numPlayers < 1)
    	{
    		numPlayers = 2;
    	}
    	if (numPlayers > 6)
    	{
    		numPlayers = 2;
    	}
    	myPlayers = new Vector<Player>();
    	if(numPlayers==1)
    	{
    		myPlayers.add(new Player());
    		myPlayers.add(new ComputerPlayer("Computer"));
    		//myPlayers.get(1).setName("Computer");
    	}
    	else
    	{
    		for (int i = 0; i < numPlayers; i++)
    		{
    			myPlayers.add(new Player());
    		} 	
    	}
    }
    
    
    /**
     * starts the game by setting up the first player
     * and roller.
     * 
     * Also reset number of rolls used
     */
    public void startGame()
    {
    	myPlayerUp = myPlayers.get(0);
    	myRoller = new Roller(5);
    	resetNumberRollsUsed(); 
    	myGameFinished = false;
    }
    
    
    /**
     * sets up player playing to the player
     * playing this turn
     * 
     * @param player the player playing this turn
     */
    public void selectPlayerUp(Player player)
    {
    	myPlayerUp = player;
    }
    
    
    /**
     * switch player to the next player 
     */
    public void switchPlayerUp()
    {
    	if (myPlayerUp == myPlayers.get(0))
    	{
    		myPlayerUp = myPlayers.get(1);
    	}
    	else if (myPlayerUp == myPlayers.get(1))
    	{
    		myPlayerUp = myPlayers.get(0);
    	}
    	myNumberRollsUsed = 0;
    }
    
    
    /**
     * add up the rolls used in one turn
     * 
     * @return if the player can roll another 
     * time or not
     */
    public boolean incrementRollsUsed()
    {
        if(myNumberRollsUsed<=MAX_ROLLS)
        {
        	myNumberRollsUsed++;
        	return true;
        }
        resetNumberRollsUsed();
        return false;
    }
    
    
    /**
     * method resets number of rolls used 
     * for the turn to value 0
     */
    public void resetNumberRollsUsed()
    {
    	myNumberRollsUsed=0;
    }
    
    
    /**
     * Gets the roller the player uses
     * 
     * @return myRoller the roller the player uses
     */
    public Roller getRoller()
    {
        return myRoller;
    }
    
    
    /**
     * Gets player playing this turn
     * 
     * @return myPlayer player playing for the turn
     */
    public Player getPlayerUp()
    {
        return myPlayerUp;
    }
    
    
    /**
     * Gets vector of all players
     * 
     * @return myPlayers the vector of all players
     */
    public Vector<Player> getPlayers()
    {	
        return myPlayers;
    }
    
    
    /**
     * Gets number of rolls used this turn
     * 
     * @return myNumberRollsUsed number of 
     * rolls used this turn
     */
    public int getNumberRollsUsed()
    {
        return myNumberRollsUsed;
    }
    
    /**
     * Sets the the game to be finished so the player cannot roll
     * the dice anymore.
     */
    public void finishGame()
    {
    	myGameFinished = true;
    }
    
    /**
     * Getter that sees if the game has concluded.
     * @return if the game is finished or not
     */
    public boolean isMyGameFinished() {
		return myGameFinished;
	}

	/**
     * Shows information of yahtzee engine
     * class.
     * 
     *  @return information of yahtzee engine
     */
    public String toString()
    {
        return "Player: "+myPlayerUp+"	number of rolls used: " +myNumberRollsUsed;
    }
}
