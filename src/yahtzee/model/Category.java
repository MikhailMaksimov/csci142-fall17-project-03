package yahtzee.model;

import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * Catagory class for the yahtzee game. 
 * 
 * @author Mikhail Maksimov
 * @author Michael Chen
 */
public class Category
{
    public static final int NO_VALUE = 0;

    private CategoryType myType;
    private String myName;
    private int myValue;
    private boolean myIsFilled;
    
    /**
     * Default constructor for Category objects
     * 
     * @param type The type of category used ex: ONES, TWOS etc.
     */
    public Category(CategoryType type)
    {
    	myType = type;
    	myValue = NO_VALUE;
    	if(type != null)
    	{
    		myName = type.getName();
    	}
    }

    /**
     * Fills the category for the right type. Takes the type and 
     * values of dice to calculate the score for the category
     * 
     * @param nums The integer array of face values of the dice
     * @return True if the category can be filled or false if it cannot be filled
     */
    public boolean fillCategoryValue(int[] nums)
    {
    	if(myType!=null)
    	{	
    		if(myType == CategoryType.ONES)
    		{
    			if(CheckOnes(nums)==true)
    			{
    				for(int i=0; i<nums.length; i++)
       	    		{
        				if(nums[i]==1)
        				{
        					myValue++;
        				}
       	    		}
    				myIsFilled=true;
    			}
    		}
    		if(myType == CategoryType.TWOS)
    		{
    			if(CheckTwos(nums)==true)
    			{
    				for(int i=0; i<nums.length; i++)
       	    		{
        				if(nums[i]==2)
        				{
        					myValue=myValue+2;
        				}
       	    		}
    				myIsFilled=true;
    			}
    		}
    		if(myType == CategoryType.THREES)
    		{
    			if(CheckThrees(nums)==true)
    			{
    				for(int i=0; i<nums.length; i++)
       	    		{
        				if(nums[i]==3)
        				{
        					myValue=myValue+3;
        				}
       	    		}
    				myIsFilled=true;
    			}
    		}
    		if(myType == CategoryType.FOURS)
    		{
    			if(CheckFours(nums)==true)
    			{
    				for(int i=0; i<nums.length; i++)
       	    		{
        				if(nums[i]==4)
        				{
        					myValue=myValue+4;
        				}
       	    		}
    				myIsFilled=true;
    			}
    		}
    		if(myType == CategoryType.FIVES)
    		{
    			if(CheckFives(nums)==true)
    			{
    				for(int i=0; i<nums.length; i++)
       	    		{
        				if(nums[i]==5)
        				{
        					myValue=myValue+5;
        				}
       	    		}
    				myIsFilled=true;
    			}
    		}
    		if(myType == CategoryType.SIXES)
    		{
    			if(CheckSixes(nums)==true)
    			{
    				for(int i=0; i<nums.length; i++)
       	    		{
        				if(nums[i]==6)
        				{
        					myValue=myValue+6;
        				}
       	    		}
    				myIsFilled=true;
    			}
    		}
    		if(myType == CategoryType.THREE_OF_KIND)
    		{
    			if(CheckThreeKind(nums)==true)
    			{
    				myValue=IntStream.of(nums).sum();
    				myIsFilled=true;
    			}
    		}
    		if(myType == CategoryType.FOUR_OF_KIND)
    		{
    			if(CheckFourKind(nums)==true)
    			{
    				myValue=IntStream.of(nums).sum();
    				myIsFilled=true;
    			}
    		}
    		if(myType == CategoryType.FULL_HOUSE)
    		{
    			if(CheckFullHouse(nums)==true)
    			{
    				myValue=25;
    				myIsFilled=true;
    			}
    		}
    		if(myType == CategoryType.SMALL_STRAIGHT)
    		{
    			if(CheckSS(nums)==true)
    			{
    				myValue=30;
    				myIsFilled=true;
    			}
    		}
    		if(myType == CategoryType.LARGE_STRAIGHT)
    		{
    			if(CheckLS(nums)==true)
    			{
    				myValue=40;
    				myIsFilled=true;
    			}
    		}
    		if(myType == CategoryType.YAHTZEE)
    		{
    			if(CheckYahtzee(nums)==true)
    			{
    				myValue=50;
    				myIsFilled=true;
    			}
    		}
    		if(myType == CategoryType.CHANCE)
    		{
    			if(CheckChance()==true)
    			{
    				myValue=IntStream.of(nums).sum();
    				myIsFilled=true;
    			}
    		}
    	}
    		
    	for(int i=0; i<nums.length; i++) //checks if dice values are in range
    	{
    		if(nums[i]<1||nums[i]>6)
   			{
    			myValue=0;
    		}
    	}
    	return myIsFilled;
    }
    /**
     * check whether it gets a value for the category
     * 
     * @param nums the dice values
     * @return whether it gets a value for the yahtzee category 
     */
    public boolean CheckYahtzee(int[] nums)
    {
    	Arrays.sort(nums);
    	if(nums[0]==nums[1]&&nums[1]==nums[2]&&nums[2]==nums[3]&&nums[3]==nums[4])
		{
			return true;
		}
    	return false;
    }
    /**
     * check whether it gets a value for the category
     * 
     * @param nums the dice values
     * @return whether it gets a value for the large straight category 
     */
    public boolean CheckLS(int[] nums)
    {
    	Arrays.sort(nums);
    	if(nums[0]==1&&nums[1]==2&&nums[2]==3&&nums[3]==4&&nums[4]==5)
		{
    		return true;
		}
		if(nums[0]==2&&nums[1]==3&&nums[2]==4&&nums[3]==5&&nums[4]==6)
		{
			return true;
		}
		return false;
    }
    /**
     * check whether it gets a value for the category
     * 
     * @param nums the dice values
     * @return whether it gets a value for the small straight category 
     */
    public boolean CheckSS(int[] nums)
    {
    	Arrays.sort(nums);
    	int check_1=0;
		int check_2=0;
		int check_3=0;
		int check_4=0;
		int check_5=0;
		int check_6=0;
		
		for(int i=0; i<nums.length; i++)
		{
			if(nums[i]==1)
			{
				check_1++;
			}
			if(nums[i]==2)
			{
				check_2++;
			}
			if(nums[i]==3)
			{
				check_3++;
			}
			if(nums[i]==4)
			{
				check_4++;
			}
			if(nums[i]==5)
			{
				check_5++;
			}
			if(nums[i]==6)
			{
				check_6++;
			}
		}
		
		if(check_1>=1&&check_2>=1&&check_3>=1&&check_4>=1)
		{
			return true;
		}
		if(check_2>=1&&check_3>=1&&check_4>=1&&check_5>=1)
		{
			return true;
		}
		if(check_3>=1&&check_4>=1&&check_5>=1&&check_6>=1)
		{
			return true;
		}
			return false;
	}
    /**
     * check whether it gets a value for the category
     * 
     * @param nums the dice values
     * @return whether it gets a value for the full house category 
     */
    public boolean CheckFullHouse(int[] nums)
    {
    	Arrays.sort(nums);
    	if(nums[0]==nums[1]&&nums[2]==nums[3]&&nums[3]==nums[4])
		{
			return true;
		}
		if(nums[0]==nums[1]&&nums[1]==nums[2]&&nums[3]==nums[4])
		{
			return true;
		}
		return false;
    }
    /**
     * check whether it gets a value for the category
     * 
     * @param nums the dice values
     * @return whether it gets a value for the chance category 
     */
    public boolean CheckChance()
    {
    	return true;
    }
    /**
     * check whether it gets a value for the category
     * 
     * @param nums the dice values
     * @return whether it gets a value for the three of a kind category 
     */
    public boolean CheckThreeKind(int[] nums)
    {
    	Arrays.sort(nums);
    	if(nums[0]==nums[1]&&nums[1]==nums[2])
		{
			return true;
		}
		if(nums[1]==nums[2]&&nums[2]==nums[3])
		{
			return true;
		}
		if(nums[2]==nums[3]&&nums[3]==nums[4])
		{
			return true;
		}
		return false;
    }
    /**
     * check whether it gets a value for the category
     * 
     * @param nums the dice values
     * @return whether it gets a value for the four of a kind category 
     */
    public boolean CheckFourKind(int[] nums)
    {
    	Arrays.sort(nums);
    	if(nums[0]==nums[1]&&nums[1]==nums[2]&&nums[2]==nums[3])
		{
			return true;
		}
		if(nums[1]==nums[2]&&nums[2]==nums[3]&&nums[3]==nums[4])
		{
			return true;
		}
		return false;
    }
    /**
     * check whether it gets a value for the category
     * 
     * @param nums the dice values
     * @return whether it gets a value for the ones category 
     */
    public boolean CheckOnes(int[] nums)
    {
    	Arrays.sort(nums);
    	if(nums[0]==1)
    	{
    		return true;
    	}
    	return false;
    }
    /**
     * check whether it gets a value for the category
     * 
     * @param nums the dice values
     * @return whether it gets a value for the twos category 
     */
    public boolean CheckTwos(int[] nums)
    {
    	if(nums[0]==2||nums[1]==2||nums[2]==2||nums[3]==2||nums[4]==2)
    	{
    		return true;
    	}
    	return false;
    }
    /**
     * check whether it gets a value for the category
     * 
     * @param nums the dice values
     * @return whether it gets a value for the threes category 
     */
    public boolean CheckThrees(int[] nums)
    {
    	if(nums[0]==3||nums[1]==3||nums[2]==3||nums[3]==3||nums[4]==3)
    	{
    		return true;
    	}
    	return false;
    }
    /**
     * check whether it gets a value for the category
     * 
     * @param nums the dice values
     * @return whether it gets a value for the fours category 
     */
    public boolean CheckFours(int[] nums)
    {
    	if(nums[0]==4||nums[1]==4||nums[2]==4||nums[3]==4||nums[4]==4)
    	{
    		return true;
    	}
    	return false;
    }
    /**
     * check whether it gets a value for the category
     * 
     * @param nums the dice values
     * @return whether it gets a value for the fives category 
     */
    public boolean CheckFives(int[] nums)
    {
    	if(nums[0]==5||nums[1]==5||nums[2]==5||nums[3]==5||nums[4]==5)
    	{
    		return true;
    	}
    	return false;
    }
    /**
     * check whether it gets a value for the category
     * 
     * @param nums the dice values
     * @return whether it gets a value for the sixes category 
     */
    public boolean CheckSixes(int[] nums)
    {
    	Arrays.sort(nums);
    	if(nums[4]==6)
    	{
    		return true;
    	}
    	return false;
    }
    /**
     * Return if the category has been filled or not
     * 
     * @return myIsFilled True if the category has been filled
     * and false if not
     */
    public boolean getIsFilled()
    {
        return myIsFilled;
    }
    
    /**
     * Gets the value of the category object
     * 
     * @return myValue Value of the category
     */
    public int getValue()
    {
        return myValue;
    }
    
    /**
     * Gets the name of the category object
     * 
     * @return myName The category name
     */
    public String getName()
    {
        return myName;
    }
    
    /**
     * Gets the type of the category object
     * 
     * @return myType Type of category
     */
    public CategoryType getType()
    {
        return myType;
    }
    
    /**
     * Prints a string of useful information when the name
     * of the category is put into a println function;
     * 
     * @return useful string of information
     */
    public String toString()
    {
        return "type: " + myType + "		" + "value: " + myValue + "	" + "filled: " + myIsFilled;
    }
    
    /**
     * Clones a category object with the same characteristics.
     * 
     * @return copy Clone of the original category object
     */
    public Object clone()
    {
        Category copy;
        copy = new Category(this.getType());
    	copy.myName = this.getName();
    	copy.myIsFilled = this.myIsFilled;
    	copy.myType = this.getType();
    	copy.myValue = this.getValue();
    	return copy;
    }
}
