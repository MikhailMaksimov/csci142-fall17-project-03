package yahtzee.model;

import java.util.Arrays;
import java.util.Vector;

/**
 * This is the roller class for the yahtzee game. It rolls
 * the dice and records the dice values.
 * 
 * @author Mikhail Maksimov 
 * @author Michael Chen
 */
public class Roller
{
    private Vector<Die> myDice;
    private int myNumberOfDice;
    private boolean[] myToRoll;
    
    
    /**
     * Constructor for the Roller with the input of the number of dice.
     * It initializes the vector of dice and fills it with dice.
     * 
     * @param numDice Max number of dice that can be rolled
     */
    public Roller(int numDice)
    {
    	myNumberOfDice = numDice;
    	myDice = new Vector<Die>(numDice);
    	myToRoll = new boolean [myNumberOfDice];
    	Arrays.fill(myToRoll, Boolean.TRUE);
    	for (int i = 0; i < numDice; i++)
    	{
    		myDice.add(new Die());
    	}
    }
    
    
    /**
     * Returns the vector for the dice.
     * 
     * @return myDice Vector for storing dice values
     */
    public Vector<Die> getDice()
    {
        return myDice;
    }

    
    /**
     * Retrieves the dice values and stores them in an array.
     * 
     * @return diceValues Array of dice values
     */
    public int[] getDiceValues()
    {
    	int[] diceValues = new int [myNumberOfDice];
    	for (int i = 0; i < diceValues.length; i++)
    	{
    		Die die = myDice.get(i);
    		int value = die.getFaceValue();;
    		diceValues[i] = value;
    	}
        return diceValues;
    }
    
    
    /**
     * Toggles a particular die to be rolled or kept
     * 
     * @param die The index of the die
     */
    public void toggleToRoll(int die)
    {
    	if (myToRoll[die]==true)
    		myToRoll[die]=false;
    	else if (myToRoll[die]==false)
        	myToRoll[die]=true;
    }
    
    /**
     * Sets dice to be rolled based on an array of booleans.
     * True means the die will be rolled.
     * 
     * @param array Array of booleans
     */
    public void setToRoll(boolean[] array)
    {
    	myToRoll = array;
    }
    
    /**
     * Returns the dice that are selected to be rolled
     * 
     * @return Array of booleans that say true if it the die should
     * be rolled or false if the die should not be rolled.
     */
    public boolean[] getToRoll()
    {
    	return myToRoll;
    }
    
    /**
     * Rolls some of the dice based on which dice are
     * selected to roll in the boolean array myToRoll.
     */
    public void roll()
    {
    	for (int i = 0; i < myDice.size(); i++)
    	{
    		if (myToRoll[i] == true)
    		{
    			Die die = myDice.get(i);
        		die.roll();
    		}
    	}
    }
    
    
    /**
     * Returns number of dice used for the yahtzee game. Usually 5 dice.
     * 
     * @return myNumberOfDice
     */
    public int getNumberOfDice()
    {
        return myNumberOfDice;
    }
    
    
    /**
     * Class for printing out a string when the name of a Roller object
     * is put into a println function.
     * 
     * @return String What I want it to say
     */
    public String toString()
    {
        return "number of dice: " + myNumberOfDice;
    
	}
}
