package yahtzee.model;

import java.util.Vector;

/**
 * ScoreCard class for the yahtzee game. This class manages
 * everything to do with the score card assigned to each player
 * to play the game and record scores.
 * 
 * @author Mikhail Maksimov
 * @author Michael Chen
 */
public class ScoreCard
{
    public static final int NUMBER_BOTTOM_CATEGORIES = 7;
    public static final int NUMBER_TOP_CATEGORIES = 6;
    public static final int NUMBER_CATEGORIES_TOTAL = 13;
    public static final int UPPER_BONUS = 63;

    private Vector<Category> myUpperCategories;
    private Vector<Category> myLowerCategories;

    private int myUpperScore;
    private int myUpperTotal;
    private int myLowerTotal;
    private int myGrandTotal;

    private int myNumberCategoriesFilled;

    
    /**
     * Constructor for the core card. sets all values to 0.
     * Fill in null for the vectors myUpperCategories and
     * myLowerCategories.
     */
    public ScoreCard()
    {
    	myUpperScore = 0;
    	myUpperTotal = 0;
    	myLowerTotal = 0;
    	myGrandTotal = 0;
    	myNumberCategoriesFilled = 0;
    	
    	myUpperCategories = new Vector<Category>();
    	myLowerCategories = new Vector<Category>();
    	
    	for (int i = 0; i < NUMBER_TOP_CATEGORIES; i++)
    	{
    		myUpperCategories.add(null);
    	}
    	for (int i = 0; i < NUMBER_BOTTOM_CATEGORIES; i++)
    	{
    		myLowerCategories.add(null);
    	}
    }
    
    
    /**
     * Resets all values to 0.
     */
    public void resetScoreCard()
    {
    	myUpperScore = 0;
    	myUpperTotal = 0;
    	myLowerTotal = 0;
    	myGrandTotal = 0;
    	myNumberCategoriesFilled = 0;
    }

    
    /**
     * Fills a category on the score sheet with a score.
     * Increments number of categories filled by one every 
     * time a category is filled and gets upper and lower score.
     * 
     * @param type category type used
     * @param values the array of dice values
     * @return true if category has not been filled
     */
    public boolean fillCategory(CategoryType type, int[] values) 
    {
    	Category cat = new Category(type);
    	
    	if(type!=null)
    	{
    		cat.fillCategoryValue(values);
    	}
    	if(type==CategoryType.ONES)
    	{
    		if(myUpperCategories.get(0)==null)
    		{
	    		myUpperCategories.set(0,cat);
	    		myNumberCategoriesFilled++;
	    		myUpperScore += cat.getValue();
	    		return true;
    		}
    	}
    	if(type==CategoryType.TWOS)
    	{
    		if(myUpperCategories.get(1)==null)
    		{
    			myUpperCategories.set(1,cat);
    			myNumberCategoriesFilled++;
    			myUpperScore += cat.getValue();
    			return true;
    		}
    	}
    	if(type==CategoryType.THREES)
    	{
    		if(myUpperCategories.get(2)==null)
    		{
    			myUpperCategories.set(2,cat);
    			myNumberCategoriesFilled++;
    			myUpperScore += cat.getValue();
    			return true;
    		}
    	}
    	if(type==CategoryType.FOURS)
    	{
    		if(myUpperCategories.get(3)==null)
    		{
    			myUpperCategories.set(3,cat);
    			myNumberCategoriesFilled++;
    			myUpperScore += cat.getValue();
    			return true;
    		}
    	}
    	if(type==CategoryType.FIVES)
    	{
    		if(myUpperCategories.get(4)==null)
    		{
    			myUpperCategories.set(4,cat);
    			myNumberCategoriesFilled++;
    			myUpperScore += cat.getValue();
    			return true;
    		}
    	}
    	if(type==CategoryType.SIXES)
    	{
    		if(myUpperCategories.get(5)==null)
    		{
    			myUpperCategories.set(5,cat);
    			myNumberCategoriesFilled++;
    			myUpperScore += cat.getValue();
    			return true;
    		}
    	}
    	if(type==CategoryType.THREE_OF_KIND)
    	{
    		if(myLowerCategories.get(0)==null)
    		{
    			myLowerCategories.set(0,cat);
    			myNumberCategoriesFilled++;
    			myLowerTotal += cat.getValue();
    			return true;
    		}
    	}
    	if(type==CategoryType.FOUR_OF_KIND)
    	{
    		if(myLowerCategories.get(1)==null)
    		{
    			myLowerCategories.set(1,cat);
    			myNumberCategoriesFilled++;
    			myLowerTotal += cat.getValue();
    			return true;
    		}
    	}
    	if(type==CategoryType.FULL_HOUSE)
    	{
    		if(myLowerCategories.get(2)==null)
    		{
    			myLowerCategories.set(2,cat);
    			myNumberCategoriesFilled++;
    			myLowerTotal += cat.getValue();
    			return true;
    		}
    	}
    	if(type==CategoryType.SMALL_STRAIGHT)
    	{
    		if(myLowerCategories.get(3)==null)
    		{
    			myLowerCategories.set(3,cat);
    			myNumberCategoriesFilled++;
    			myLowerTotal += cat.getValue();
    			return true;
    		}
    	}
    	if(type==CategoryType.LARGE_STRAIGHT)
    	{
    		if(myLowerCategories.get(4)==null)
    		{
    			myLowerCategories.set(4,cat);
    			myNumberCategoriesFilled++;
    			myLowerTotal += cat.getValue();
    			return true;
    		}
    	}
    	if(type==CategoryType.YAHTZEE)
    	{
    		if(myLowerCategories.get(5)==null)
    		{
    			myLowerCategories.set(5,cat);
    			myNumberCategoriesFilled++;
    			myLowerTotal += cat.getValue();
    			return true;
    		}
    	}
    	if(type==CategoryType.CHANCE)
    	{
    		if(myLowerCategories.get(6)==null)
    		{
    			myLowerCategories.set(6,cat);
    			myNumberCategoriesFilled++;
    			myLowerTotal += cat.getValue();
    			return true;
    		}
    	}
    	return false;
    }
    /**
     * Gets correct category object from vector
     * based on category type used.
     * 
     * @param type category type being used
     * @return the appropriate category stored in
     * vector
     */
    public Category getCategory(CategoryType type)
    {
    	Category cat = new Category(type);
    	if (type == CategoryType.ONES)
    	{
    		if(myUpperCategories.get(0)!=null)
    		{
    			cat=myUpperCategories.get(0);
    		}
    	}
    	if (type == CategoryType.TWOS)
    	{
    		if(myUpperCategories.get(1)!=null)
    		{
    			cat=myUpperCategories.get(1);
    		}
    	}
    	if (type == CategoryType.THREES)
    	{
    		if(myUpperCategories.get(2)!=null)
    		{
    			cat=myUpperCategories.get(2);
    		}
    	}
    	if (type == CategoryType.FOURS)
    	{
    		if(myUpperCategories.get(3)!=null)
    		{
    			cat=myUpperCategories.get(3);
    		}
    	}
    	if (type == CategoryType.FIVES)
    	{
    		if(myUpperCategories.get(4)!=null)
    		{
    			cat=myUpperCategories.get(4);
    		}
    	}
    	if (type == CategoryType.SIXES)
    	{
    		if(myUpperCategories.get(5)!=null)
    		{
    			cat=myUpperCategories.get(5);
    		}
    	}
    	if (type == CategoryType.THREE_OF_KIND)
    	{
    		if(myLowerCategories.get(0)!=null)
    		{
    			cat=myLowerCategories.get(0);
    		}
    	}
    	if (type == CategoryType.FOUR_OF_KIND)
    	{
    		if(myLowerCategories.get(1)!=null)
    		{
    			cat=myLowerCategories.get(1);
    		}
    	}
    	if (type == CategoryType.FULL_HOUSE)
    	{
    		if(myLowerCategories.get(2)!=null)
    		{
    			cat=myLowerCategories.get(2);
    		}
    	}
    	if (type == CategoryType.SMALL_STRAIGHT)
    	{
    		if(myLowerCategories.get(3)!=null)
    		{
    			cat=myLowerCategories.get(3);
    		}
    	}
    	if (type == CategoryType.LARGE_STRAIGHT)
    	{
    		if(myLowerCategories.get(4)!=null)
    		{
    			cat=myLowerCategories.get(4);
    		}
    	}
    	if (type == CategoryType.YAHTZEE)
    	{
    		if(myLowerCategories.get(5)!=null)
    		{
    			cat=myLowerCategories.get(5);
    		}
    	}
    	if (type == CategoryType.CHANCE)
    	{
    		if(myLowerCategories.get(6)!=null)
    		{
    			cat=myLowerCategories.get(6);
    		}
    	}
        return cat;
    }
    
    /**
     * Getter for the upper categories
     * @return myUpperCategories;
     */
    public Vector<Category> getMyUpperCategories() {
		return myUpperCategories;
	}
    
    /**
     * Getter for the lower categories
     * @return myLowerCategories
     */
	public Vector<Category> getMyLowerCategories() {
		return myLowerCategories;
	}


	/**
     * Gets the number of categories filled on
     * score card
     * 
     * @return myNumberCategoriesFilled number of categories filled
     */
    public int getNumberCategoriesFilled()
    {
        return myNumberCategoriesFilled;
    }
    
    
    /**
     * Gets sum of the scores of upper  categories
     * 
     * @return myUpperScore the sum of the scores
     * of the upper categories
     */
    public int getUpperScore()
    {
        return myUpperScore;
    }
    
    
    /**
     * Gets the total score of the upper categories
     * considering if bonus condition is met or not
     * 
     * @return myUpperTotal the total score of
     * the upper categories
     */
    public int getUpperTotal()
    {
    	if(myUpperScore>=UPPER_BONUS)
    	{
    		myUpperTotal=myUpperScore+35;
    	}
    	else
    	{
    		myUpperTotal = myUpperScore;
    	}
        return myUpperTotal;
    }
    
    
    /**
     * Gets the total score of the lower categories
     * 
     * @return myUpperTotal the total score of
     * the upper categories
     */
    public int getLowerTotal()
    {
        return myLowerTotal;
    }
    
    
    /**
     * Gets total score of score card by adding
     * lower category total to upper category total
     * 
     * @return myGrandTotal the total score of the
     * score card
     */
    public int getGrandTotal()
    {
    	myGrandTotal=getUpperTotal()+getLowerTotal();
        return myGrandTotal;
    }
    
    
    /**
     * shows information of score card
     * 
     * @return information of score card
     */
    public String toString()
    {
        return "upper total: "+myUpperTotal+ "	lower total: "+myLowerTotal+"	grand total: "+myGrandTotal;
    }
    
    
    /**
     * Creates a clone of the score card
     * 
     * @return copy a clone of original score
     * card that has same properties and scores
     */
    public Object clone()
    {
    	ScoreCard copy;
        copy = new ScoreCard();
    	copy.myNumberCategoriesFilled = this.getNumberCategoriesFilled();
    	copy.myUpperScore = this.getUpperScore();
    	copy.myUpperTotal = this.getUpperTotal();
    	copy.myLowerTotal = this.getLowerTotal();
    	copy.myGrandTotal = this.getGrandTotal();
    	return copy;
    }
}
