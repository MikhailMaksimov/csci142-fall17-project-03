package yahtzee.model;

import java.util.Arrays;
import java.util.Vector;

/**
 * Class for the computer player AI
 * 
 * 
 * @author Mikhail Maksimov
 * @author Michael Chen
 */

public class ComputerPlayer extends Player
{
	private int numDice=5; // number of dice
	private Roller myRoll;	// roller to roll dice
	private ScoreCard mycpScoreCard;	// score card to record categories
	private String myName;	// name of computer player
	private int myNumberOfRolls;	// number of rolls
	private boolean myFilled;	// filled categories
	private CategoryType myType;	// type of category 
	private boolean[] myRerollDice; // select dice to reroll
	private int[] mySortedValues;	// sorted dice values
	private Category myCat;	// category for checking category validity
	private Vector<Category> myUpperCat;	// vector of upper categories
	private Vector<Category> myLowerCat;	// vector of lower categories
	
	public ComputerPlayer(String name)
	{
		 myName=name;	// sets name of computer player
		 myNumberOfRolls=0; 	// initialize number of rolls
		 myFilled=false;	// set filled categories to false
		 myRerollDice=new boolean[5];	// initialize dice reroll array
		 mycpScoreCard=new ScoreCard();	// initialize score card
		 myRoll=new Roller(numDice);	// initialize roller
		 myCat= new Category(CategoryType.CHANCE); // initialize category
		 myUpperCat= mycpScoreCard.getMyUpperCategories();	// set scorecards category vector
		 myLowerCat= mycpScoreCard.getMyLowerCategories();	// set scorecards category vector
		 
	}
	
	/**
	 * Check if computer player should roll again or not
	 * 
	 * @param values the dice values gotten
	 * @return if the dice should be rolled or not
	 */
	public boolean rollAgain(int[] values)
    {
		myFilled=false;
        if(myNumberOfRolls==0)
        {
        	myNumberOfRolls++;
        	myRoll.roll();
        	return true;
        }
        if(myNumberOfRolls<3)
        {
        	if(myCat.CheckYahtzee(values)==true&&myLowerCat.get(5)==null)
        	{
        		selectCategoryToFill(values);
        		return false;
        	}
        	if(myCat.CheckLS(values)==true&&myLowerCat.get(4)==null)
        	{
        		selectCategoryToFill(values);
        		return false;
        	}
        	if(myCat.CheckSS(values)==true&&myLowerCat.get(3)==null)
        	{
        		selectCategoryToFill(values);
        		return false;
        	}
        	if(myCat.CheckFullHouse(values)==true&&myLowerCat.get(2)==null)
        	{
        		selectCategoryToFill(values);
        		return false;
        	}
        	selectDiceToReroll(values);
        	myRoll.setToRoll(myRerollDice);
        	myRoll.roll();
        	myNumberOfRolls++;
        	return true;
        }
        selectCategoryToFill(values);
        return false;
    }
	/**
	 * Select which dice should be rolled or not
	 * 
	 * @param values the dice values gotten
	 * @return rerollDice which of the dice should be rolled or not 
	 */
    public boolean[] selectDiceToReroll(int[] values)
    {
    	mySortedValues = new int[5];
    	for(int i = 0; i < numDice; i++)
    	{
    		mySortedValues[i] = values[i];
    	}
    	
    	Arrays.sort(mySortedValues);
        // Keeps the die value that repeats most and have highest value
        if(mySortedValues[0]==mySortedValues[1]&&mySortedValues[1]==mySortedValues[2]&&mySortedValues[2]==mySortedValues[3])
        {
        	for(int i=0;i<numDice;i++)
        	{
        		if(values[i]!=mySortedValues[0])
        		{
        			myRerollDice[i]=true;
        		}
        	}
        	return myRerollDice;
        }
        if(mySortedValues[1]==mySortedValues[2]&&mySortedValues[2]==mySortedValues[3]&&mySortedValues[3]==mySortedValues[4])
        {
        	for(int i=0;i<numDice;i++)
        	{
        		if(values[i]!=mySortedValues[1])
        		{
        			myRerollDice[i]=true;
        		}
        	}
        	return myRerollDice;
        }
        if(mySortedValues[0]==mySortedValues[1]&&mySortedValues[1]==mySortedValues[2])
        {
        	for(int i=0;i<numDice;i++)
        	{
        		if(values[i]!=mySortedValues[0])
        		{
        			myRerollDice[i]=true;
        		}
        	}
        	return myRerollDice;
        }
        if(mySortedValues[1]==mySortedValues[2]&&mySortedValues[2]==mySortedValues[3])
        {
        	for(int i=0;i<numDice;i++)
        	{
        		if(values[i]!=mySortedValues[1])
        		{
        			myRerollDice[i]=true;
        		}
        	}
        	return myRerollDice;
        }
        if(mySortedValues[2]==mySortedValues[3]&&mySortedValues[3]==mySortedValues[4])
        {
        	for(int i=0;i<numDice;i++)
        	{
        		if(values[i]!=mySortedValues[2])
        		{
        			myRerollDice[i]=true;
        		}
        	}
        	return myRerollDice;
        }
        if(mySortedValues[3]==mySortedValues[4])
        {
        	for(int i=0;i<numDice;i++)
        	{
        		if(values[i]!=mySortedValues[3])
        		{
        			myRerollDice[i]=true;
        		}
        	}
        	return myRerollDice;
        }
        if(mySortedValues[2]==mySortedValues[3])
        {
        	for(int i=0;i<numDice;i++)
        	{
        		if(values[i]!=mySortedValues[2])
        		{
        			myRerollDice[i]=true;
        		}
        	}
        	return myRerollDice;
        }
        if(mySortedValues[1]==mySortedValues[2])
        {
        	for(int i=0;i<numDice;i++)
        	{
        		if(values[i]!=mySortedValues[1])
        		{
        			myRerollDice[i]=true;
        		}
        	}
        	return myRerollDice;
        }
        if(mySortedValues[0]==mySortedValues[1])
        {
        	for(int i=0;i<numDice;i++)
        	{
        		if(values[i]!=mySortedValues[0])
        		{
        			myRerollDice[i]=true;
        		}
        	}
        	return myRerollDice;
        }
        else
        {
        	for(int i=0;i<numDice;i++)
        	{
        		myRerollDice[i]=true;
        	}
        	return myRerollDice;
        }
    }
    /**
     * Select which category to fill
     * 
     * @param values the dice values gotten
     * @return which category to fill
     */
    public CategoryType selectCategoryToFill(int[] values)
    {
    	if(myCat.CheckYahtzee(values)==true&&myLowerCat.get(5)==null)
    	{
    		mycpScoreCard.fillCategory(CategoryType.YAHTZEE,values);
    		myFilled=true;
    		myNumberOfRolls=0;
    		myType=CategoryType.YAHTZEE;
    		return CategoryType.YAHTZEE;
    	}
    	if(myCat.CheckLS(values)==true&&myLowerCat.get(4)==null)
    	{
    		mycpScoreCard.fillCategory(CategoryType.LARGE_STRAIGHT,values);
    		myFilled=true;
    		myNumberOfRolls=0;
    		myType=CategoryType.LARGE_STRAIGHT;
    		return CategoryType.LARGE_STRAIGHT;
    	}
    	if(myCat.CheckSS(values)==true&&myLowerCat.get(3)==null)
    	{
    		mycpScoreCard.fillCategory(CategoryType.SMALL_STRAIGHT,values);
    		myFilled=true;
    		myNumberOfRolls=0;
    		myType=CategoryType.SMALL_STRAIGHT;
    		return CategoryType.SMALL_STRAIGHT;
    	}
    	if(myCat.CheckFullHouse(values)==true&&myLowerCat.get(2)==null)
    	{
    		mycpScoreCard.fillCategory(CategoryType.FULL_HOUSE,values);
    		myFilled=true;
    		myNumberOfRolls=0;
    		myType=CategoryType.FULL_HOUSE;
    		return CategoryType.FULL_HOUSE;
    	}
    	if(myCat.CheckFourKind(values)==true&&myLowerCat.get(1)==null)
    	{
    		mycpScoreCard.fillCategory(CategoryType.FOUR_OF_KIND,values);
    		myFilled=true;
    		myNumberOfRolls=0;
    		myType=CategoryType.FOUR_OF_KIND;
    		return CategoryType.FOUR_OF_KIND;
    	}
    	if(myCat.CheckThreeKind(values)==true&&myLowerCat.get(0)==null)
    	{
    		mycpScoreCard.fillCategory(CategoryType.THREE_OF_KIND,values);
    		myFilled=true;
    		myNumberOfRolls=0;
    		myType=CategoryType.THREE_OF_KIND;
    		return CategoryType.THREE_OF_KIND;
    	}
    	// get categories that have more dice for a value
    	for(int i=1; i<=6;i++)
    	{
        	int Counter=0;
    		for(int j=0; j<numDice;j++)
    		{
    			if(values[j]==i)
    			{
    				Counter++;
    			}
    		}
    		if(i==6&&Counter>1)
    		{
		    	if(myCat.CheckSixes(values)==true&&myUpperCat.get(5)==null)
		    	{
		    		mycpScoreCard.fillCategory(CategoryType.SIXES,values);
		    		myFilled=true;
		    		myNumberOfRolls=0;
		    		Counter=0;
		    		myType=CategoryType.SIXES;
		    		return CategoryType.SIXES;
		    	}
    		}
    		if(i==5&&Counter>1)
    		{
		    	if(myCat.CheckFives(values)==true&&myUpperCat.get(4)==null)
		    	{
		    		mycpScoreCard.fillCategory(CategoryType.FIVES,values);
		    		myFilled=true;
		    		myNumberOfRolls=0;
		    		Counter=0;
		    		myType=CategoryType.FIVES;
		    		return CategoryType.FIVES;
		    	}
    		}
    		if(i==4&&Counter>1)
    		{
		    	if(myCat.CheckFours(values)==true&&myUpperCat.get(3)==null)
		    	{
		    		mycpScoreCard.fillCategory(CategoryType.FOURS,values);
		    		myFilled=true;
		    		myNumberOfRolls=0;
		    		Counter=0;
		    		myType=CategoryType.FOURS;
		    		return CategoryType.FOURS;
		    	}
    		}
    		if(i==3&&Counter>1)
    		{
		    	if(myCat.CheckThrees(values)==true&&myUpperCat.get(2)==null)
		    	{
		    		mycpScoreCard.fillCategory(CategoryType.THREES,values);
		    		myFilled=true;
		    		myNumberOfRolls=0;
		    		Counter=0;
		    		myType=CategoryType.THREES;
		    		return CategoryType.THREES;
		    	}
    		}
    		if(i==2&&Counter>1)
    		{
		    	if(myCat.CheckTwos(values)==true&&myUpperCat.get(1)==null)
		    	{
		    		mycpScoreCard.fillCategory(CategoryType.TWOS,values);
		    		myFilled=true;
		    		myNumberOfRolls=0;
		    		Counter=0;
		    		myType=CategoryType.TWOS;
		    		return CategoryType.TWOS;
		    	}
    		}
    		if(i==1&&Counter>1)
    		{
		    	if(myCat.CheckOnes(values)==true&&myUpperCat.get(0)==null)
		    	{
		    		mycpScoreCard.fillCategory(CategoryType.ONES,values);
		    		myFilled=true;
		    		myNumberOfRolls=0;
		    		Counter=0;
		    		myType=CategoryType.ONES;
		    		return CategoryType.ONES;
		    	}
    		}
    	}
    	if(myCat.CheckChance()==true&&myLowerCat.get(6)==null)
    	{
    		mycpScoreCard.fillCategory(CategoryType.CHANCE,values);
    		myFilled=true;
    		myNumberOfRolls=0;
    		myType=CategoryType.CHANCE;
    		return CategoryType.CHANCE;
    	}
    	// Get categories that have not been filled
    	for(int i=0;i<myLowerCat.size();i++)
    	{
   			if(myLowerCat.get(i)==null)
    		{
    			if(i==0)
    			{
    				mycpScoreCard.fillCategory(CategoryType.THREE_OF_KIND,values);
    		   		myFilled=true;
    		   		myNumberOfRolls=0;
    		   		myType=CategoryType.THREE_OF_KIND;
    		   		return CategoryType.THREE_OF_KIND;
    			}
    			if(i==1)
    			{
    				mycpScoreCard.fillCategory(CategoryType.FOUR_OF_KIND,values);
    		    	myFilled=true;
    		    	myNumberOfRolls=0;
    		   		myType=CategoryType.FOUR_OF_KIND;
    		    	return CategoryType.FOUR_OF_KIND;
    			}
    			if(i==2)
    			{
    				mycpScoreCard.fillCategory(CategoryType.FULL_HOUSE,values);
    	    		myFilled=true;
    		    	myNumberOfRolls=0;
    		   		myType=CategoryType.FULL_HOUSE;
    		   		return CategoryType.FULL_HOUSE;
    			}
    			if(i==3)
    			{
    				mycpScoreCard.fillCategory(CategoryType.SMALL_STRAIGHT,values);
    	    		myFilled=true;
    		    	myNumberOfRolls=0;
    		   		myType=CategoryType.SMALL_STRAIGHT;
    		   		return CategoryType.SMALL_STRAIGHT;
    			}
    			if(i==4)
    			{
    				mycpScoreCard.fillCategory(CategoryType.LARGE_STRAIGHT,values);
    	    		myFilled=true;
    		   		myNumberOfRolls=0;
    		   		myType=CategoryType.LARGE_STRAIGHT;
    		   		return CategoryType.LARGE_STRAIGHT;
    			}
				if(i==5)
				{
					mycpScoreCard.fillCategory(CategoryType.YAHTZEE,values);
		    		myFilled=true;
		    		myNumberOfRolls=0;
		    		myType=CategoryType.YAHTZEE;
		    		return CategoryType.YAHTZEE;
				}
			}
    	}
		for(int g=0;g<myUpperCat.size();g++)
    	{
   			if(myUpperCat.get(g)==null)
    		{
    			if(g==0)
    			{
    				mycpScoreCard.fillCategory(CategoryType.ONES,values);
    		   		myFilled=true;
    		   		myNumberOfRolls=0;
    		   		myType=CategoryType.ONES;
    		   		return CategoryType.ONES;
    			}
    			if(g==1)
    			{
    				mycpScoreCard.fillCategory(CategoryType.TWOS,values);
    		    	myFilled=true;
    		    	myNumberOfRolls=0;
    		   		myType=CategoryType.TWOS;
    		    	return CategoryType.TWOS;
    			}
    			if(g==2)
    			{
    				mycpScoreCard.fillCategory(CategoryType.THREES,values);
    	    		myFilled=true;
    		    	myNumberOfRolls=0;
    		   		myType=CategoryType.THREES;
    		   		return CategoryType.THREES;
    			}
    			if(g==3)
    			{
    				mycpScoreCard.fillCategory(CategoryType.FOURS,values);
    	    		myFilled=true;
    		    	myNumberOfRolls=0;
    		   		myType=CategoryType.FOURS;
    		   		return CategoryType.FOURS;
    			}
    			if(g==4)
    			{
    				mycpScoreCard.fillCategory(CategoryType.FIVES,values);
    	    		myFilled=true;
    		   		myNumberOfRolls=0;
    		   		myType=CategoryType.FIVES;
    		   		return CategoryType.FIVES;
    			}
				if(g==5)
				{
					mycpScoreCard.fillCategory(CategoryType.SIXES,values);
		    		myFilled=true;
		    		myNumberOfRolls=0;
		    		myType=CategoryType.SIXES;
		    		return CategoryType.SIXES;
				}
			}
    	}
   		return null;
    }
    /**
     * gets the category filled
     * 
     * @return myType the category type filled
     */
    public CategoryType getCategoryFilled()
    {
    	return myType;
    }
    /**
     * Gets name of computer player
     * 
     * @return myName
     */
    public String getName()
    {
        return myName;
    }
    /**
     * Gets whether category has been filled this turn
     * 
     * @return myFilled whether category has been filled or not
     */
    public boolean getmyFilled()
    {
    	return myFilled;
    }
    /**
     * Sets number of rolls used
     * 
     * @param myNumberOfRolls number of rolls used
     */
	public void setMyNumberOfRolls(int myNumberOfRolls) {
		this.myNumberOfRolls = myNumberOfRolls;
	}
	/**
	 * Sets state of category filled or not
	 * 
	 * @param myFilled whether a category has been filled
	 */
	public void setMyFilled(boolean myFilled) {
		this.myFilled = myFilled;
	}
	/**
	 * Gets roller of computer player
	 * 
	 * @return myRoll the roller for computer player
	 */
	public Roller getRoller()
	{
		return myRoll;
	}
	/**
	 * Gets score card of computer player
	 * 
	 * @return mycpScoreCard the score card of computer player
	 */
	public ScoreCard sc()
	{
		return mycpScoreCard;
	}
}
