package yahtzee.model;

/**
 * This is the player class for the yahtzee game. This class
 * records the name, wins, and the score card of the player.
 * 
 * @author Mikhail Maksimov
 * @author Michael Chen
 */
public class Player
{
    public static String DEFAULT_NAME = "Player";
    
    private String myName;
    private int myNumberOfWins;
    private ScoreCard myScoreCard;
    
    
    /**
     * Default player constructor. sets "Player" as the name.
     */
    public Player()
    {
    	this(DEFAULT_NAME);
    }
    
    
    /**
     * Player constructor with a name input.
     * 
     * @param name Name of the player
     */
    public Player(String name)
    {
    	myName = name;
    	myNumberOfWins = 0;
    	myScoreCard = new ScoreCard();
    }
    
    
    /**
     * Checks that the name of the player uses only alphanumeric characters
     * (numbers and letters).
     * 
     * @param name Name of the player
     * @return boolean True if name is valid or false if it is not
     */
    public static boolean validateName(String name)
    {
    	if (name!=null)
    	{
    		if (name.matches("[a-zA-Z0-9]*"))
        	{
        		return true;
        	}
    	}
        return false;
    }
    
    
    /**
     * Increases the number of wins by 1
     */
    public void incrementWins()
    {
    	myNumberOfWins++;
    }
    
    
    /**
     * Resets the number of wins to 0.
     */
    public void resetWins()
    {
    	myNumberOfWins = 0;
    }
    
    
    /**
     * Sets the name of the player to the given name.
     * 
     * @param myName Name of the player
     */
    public void setName(String name)
    {
    	myName = name;
    }
    
    
    /**
     * Returns the name of the player.
     * 
     * @return myName Name of the player
     */
    public String getName()
    {
        return myName;
    }
    
    
    /**
     * Returns the number of wins the player has.
     * 
     * @return myNumberOfWins The number of times the player has won
     */
    public int getNumberOfWins()
    {
        return myNumberOfWins;
    }
    
    
    /**
     * Sets the score card for the player
     * 
     * @param card Score card of the player
     */
    public void setScoreCard(ScoreCard card)
    {
    	myScoreCard = card;
    }

    
    /**
     * Gets the score card of the player.
     * 
     * @return myScoreCard Score card of the player
     */
    public  ScoreCard getScoreCard()
    {
        return myScoreCard;
    }
    
    
    /**
     * Method for printing out important information about the player
     * when the player object is put into a System.out.print function.
     * 
     * @return String The text I want to see when i put this object in println
     */
    public String toString()
    {
        return "name: " + myName + "	" + "games won: " + myNumberOfWins;
    }
    
    
    /**
     * Method for cloning a Player object. The copied object has the same 
     * name and the same number of wins as the player that was copied.
     * 
     * @return copy An exact copy of a Player object
     */
    public Object clone()
    {
    	Player copy;
    	copy = new Player();
    	copy.setName(myName);
    	for (int wins = 0; wins < myNumberOfWins; wins++)
    	{
    		copy.incrementWins();	
    	}
    	return copy;
    }
}
