package yahtzee.controller;

import java.util.Vector;

import javax.swing.*;
import yahtzee.model.*;
import yahtzee.view.*;

/**
 * Controller class for the Yahtzee game.
 * Lets the model 'communicate' with the view.
 * 
 * @author Michael Chen
 * @author Mikhail Maksimov
 */
public class Controller {
	
	private View myView;
    private YahtzeeEngine myModel;
    
    private boolean myCpFilledAllUpper; //false if the computer has not filled all upper categories
    private boolean myPlayerFilledAllUpper; //false if the player has not filled all upper categories
    private boolean myCpFilledAllLower; //false if the computer has not filled all lower categories
    private boolean myPlayerFilledAllLower; //false if the player has not filled all lower categories
    
    
    /**
     * Constructor for the Controller object.
     * Initializes the model and the view.
     * Sets the name entered to be the player's name.
     */
    public Controller()
    {
    	String playerName = askForName();
    	myModel = new YahtzeeEngine(1);
    	myView = new View(this);
    	
    	Vector<Player> players = myModel.getPlayers();
    	Player player = players.get(0);
    	player.setName(playerName);
    	myView.setPlayerName(playerName);
    	myModel.startGame();
    }
    
    
    /**
     * Displays a panel for the player to enter their name.
     * Asks them again for the name if the name given in not alphanumeric.
     * Sets the name to 'Player' if nothing is entered.
     * 
     * @return playerName Name of the player
     */
    public String askForName()
    {
    	ImageIcon yahtzeeLogo = new ImageIcon("images/yahtzee_logo.png");
    	String playerName = (String)JOptionPane.showInputDialog
    			(null,"Please type in your name:","Yahtzee",JOptionPane
    					.PLAIN_MESSAGE,yahtzeeLogo, null, null);
    	if (playerName == null)
		{
			playerName = "";
		}
    	
    	while (Player.validateName(playerName) == false)
    	{
    		playerName = (String)JOptionPane.showInputDialog
        			(null,"Your name may only have letters and numbers.\n"
        			+ "Please try again:","Yahtzee",JOptionPane
        			.PLAIN_MESSAGE,yahtzeeLogo, null, null);
    		if (playerName == null)
    		{
    			playerName = "";
    		}
    	}
    	
    	if (playerName.equals(""))
    	{
    		playerName = Player.DEFAULT_NAME;
    	}
    	return playerName;
    }
    
    
    /**
     * Method for the 'Roll Dice' button in the view.
     * Rolls all the dice that are selected.
     */
    public void rollDice()
    {
    	Player playerUp = myModel.getPlayerUp();
    	Roller roller = myModel.getRoller();
    	boolean[] toRoll = roller.getToRoll();
    	if (!playerUp.getName().equals("Computer") && myModel.isMyGameFinished() == false)
    	{
    		if (!(toRoll[0]==false && toRoll[1]==false && toRoll[2]==false && toRoll[3]==false && toRoll[4]==false))
    		{
		    	if (myModel.getNumberRollsUsed() < 3)
		    	{
			    	myModel.incrementRollsUsed();
			    	displayMessage();
			    	roller.roll();
			    	Vector <Die> dice = roller.getDice();
			    	for (int i = 0; i < 5; i++)
			    	{
			    		if (toRoll[i] == true)
			    		{
			    			Die die = dice.get(i);
			    			int value = die.getFaceValue();
			    			myView.changeImage(i,value-1);
			    		}
			    	}
			    	displayPossibleScores();
		    	}
	    	}
    	}
    }
    
    
    /**
     * Displays messages that tell you how many rolls you have left.
     */
    public void displayMessage()
    {
    	if (myModel.getNumberRollsUsed() == 1)
		{
    		myView.setMessage("<html><body><center>You have 2 rolls left.<br>"
    				+ "Choose the dice you want to keep and roll again.</center></body></html>");
		}
    	if (myModel.getNumberRollsUsed() == 2)
    	{
    		myView.setMessage("<html><body><center>You have 1 roll left.<br>"
    				+ "Choose the dice you want to keep and roll again.</center></body></html>");
    	}
    	if (myModel.getNumberRollsUsed() == 3)
    	{
    		myView.setMessage("<html><body><center>You have used up all of your rolls.<br>"
    				+ "Please pick a category.</center></body></html>");
    	}
    	
    }
    
    
    /**
     * Displays possible scores for each category in red text.
     * Updates after every roll and erases old predictions.
     * Does not display values of 0.
     */
    public void displayPossibleScores()
    {
    	Vector<Player> players = myModel.getPlayers();
    	Player player = players.get(0);
    	ScoreCard playerScorecard = player.getScoreCard();
    	Vector<Category> lowerCategories = playerScorecard.getMyLowerCategories();
    	Vector<Category> upperCategories = playerScorecard.getMyUpperCategories();
    	Roller roller = myModel.getRoller();
    	
    	if (upperCategories.get(0) == null)
		{
			Category cat= new Category(CategoryType.ONES);
			cat.fillCategoryValue(roller.getDiceValues());
			if (cat.getValue()>0)
			{
				myView.showPossibleScores(0,cat.getValue());
			}
			else
			{
				myView.erasePossibleScores(0);
			}
		}
		if (upperCategories.get(1) == null)
		{
			Category cat= new Category(CategoryType.TWOS);
			cat.fillCategoryValue(roller.getDiceValues());
			if (cat.getValue()>0)
			{
				myView.showPossibleScores(1,cat.getValue());
			}
			else
			{
				myView.erasePossibleScores(1);
			}
		}
		if (upperCategories.get(2) == null)
		{
			Category cat= new Category(CategoryType.THREES);
			cat.fillCategoryValue(roller.getDiceValues());
			if (cat.getValue()>0)
			{
				myView.showPossibleScores(2,cat.getValue());
			}
			else
			{
				myView.erasePossibleScores(2);
			}
		}
		if (upperCategories.get(3) == null)
		{
			Category cat= new Category(CategoryType.FOURS);
			cat.fillCategoryValue(roller.getDiceValues());
			if (cat.getValue()>0)
			{
				myView.showPossibleScores(3,cat.getValue());
			}
			else
			{
				myView.erasePossibleScores(3);
			}
		}
		if (upperCategories.get(4) == null)
		{
			Category cat= new Category(CategoryType.FIVES);
			cat.fillCategoryValue(roller.getDiceValues());
			if (cat.getValue()>0)
			{
				myView.showPossibleScores(4,cat.getValue());
			}
			else
			{
				myView.erasePossibleScores(4);
			}
		}
		if (upperCategories.get(5) == null)
		{
			Category cat= new Category(CategoryType.SIXES);
			cat.fillCategoryValue(roller.getDiceValues());
			if (cat.getValue()>0)
			{
				myView.showPossibleScores(5,cat.getValue());
			}
			else
			{
				myView.erasePossibleScores(5);
			}
		}
		if (lowerCategories.get(0) == null)
		{
			Category cat= new Category(CategoryType.THREE_OF_KIND);
			cat.fillCategoryValue(roller.getDiceValues());
			if (cat.getValue()>0)
			{
				myView.showPossibleScores(6,cat.getValue());
			}
			else
			{
				myView.erasePossibleScores(6);
			}
		}
		if (lowerCategories.get(1) == null)
		{
			Category cat= new Category(CategoryType.FOUR_OF_KIND);
			cat.fillCategoryValue(roller.getDiceValues());
			if (cat.getValue()>0)
			{
				myView.showPossibleScores(7,cat.getValue());
			}
			else
			{
				myView.erasePossibleScores(7);
			}
		}
		if (lowerCategories.get(2) == null)
		{
			Category cat= new Category(CategoryType.FULL_HOUSE);
			cat.fillCategoryValue(roller.getDiceValues());
			if (cat.getValue()>0)
			{
				myView.showPossibleScores(8,cat.getValue());
			}
			else
			{
				myView.erasePossibleScores(8);
			}
		}
		if (lowerCategories.get(3) == null)
		{
			Category cat= new Category(CategoryType.SMALL_STRAIGHT);
			cat.fillCategoryValue(roller.getDiceValues());
			if (cat.getValue()>0)
			{
				myView.showPossibleScores(9,cat.getValue());
			}
			else
			{
				myView.erasePossibleScores(9);
			}
		}
		if (lowerCategories.get(4) == null)
		{
			Category cat= new Category(CategoryType.LARGE_STRAIGHT);
			cat.fillCategoryValue(roller.getDiceValues());
			if (cat.getValue()>0)
			{
				myView.showPossibleScores(10,cat.getValue());
			}
			else
			{
				myView.erasePossibleScores(10);
			}
		}
		if (lowerCategories.get(5) == null)
		{
			Category cat= new Category(CategoryType.YAHTZEE);
			cat.fillCategoryValue(roller.getDiceValues());
			if (cat.getValue()>0)
			{
				myView.showPossibleScores(11,cat.getValue());
			}
			else
			{
				myView.erasePossibleScores(11);
			}
		}
		if (lowerCategories.get(6) == null)
		{
			Category cat= new Category(CategoryType.CHANCE);
			cat.fillCategoryValue(roller.getDiceValues());
			if (cat.getValue()>0)
			{
				myView.showPossibleScores(12,cat.getValue());
			}
			else
			{
				myView.erasePossibleScores(12);
			}
		}
    }
    
    
    /**
     * Method for the die buttons in the view.
     * Toggles the die to be rolled or kept when it is clicked on.
     * 
     * @param index The index of the die button that was clicked
     */
    public void selectDice(Integer index)
    {
    	Player playerUp = myModel.getPlayerUp();
    	if (!playerUp.getName().equals("Computer") && myModel.isMyGameFinished() == false)
    	{
	    	if (myModel.getNumberRollsUsed() != 0)
	    	{
		    	Roller roller = myModel.getRoller();
		    	roller.toggleToRoll(index);
		    	boolean[] toRoll = roller.getToRoll();
		    	if (toRoll[index] == true)
		    	{
		    		myView.selectToRoll(index);
		    	}
		    	if (toRoll[index] == false)
		    	{
		    		myView.selectToKeep(index);
		    	}
	    	}
    	}
    }
    
    
    /**
     * Method that lets the computer roll the dice and pick a category.
     * Displays message that tells you what what category the computer filled.
     */
    public void computerTurn()
    {
    	Vector <Player> players = myModel.getPlayers();
    	ComputerPlayer computer = (ComputerPlayer) players.get(1);
    	ScoreCard scorecard = computer.sc();
    	computer.setMyFilled(false);
    	
    	while (computer.getmyFilled() == false)
    	{
    		Roller roller = computer.getRoller();
        	int[] values = roller.getDiceValues();
    		computer.rollAgain(values);
    	}
    	myModel.switchPlayerUp();
    	Category category = scorecard.getCategory(computer.getCategoryFilled());
    	int value = category.getValue();
    	
    	myView.setMessage("<html><body><center>The computer filled the category '"
    	+ computer.getCategoryFilled().getName() + "' with a score of "
    	+ value + ".<br>Please roll again.</center></body></html>");
    	
    	if (computer.getCategoryFilled() == CategoryType.ONES)
    	{
    		myView.setComputerScore(0, value);
    	}
    	if (computer.getCategoryFilled() == CategoryType.TWOS)
    	{
    		myView.setComputerScore(1, value);
    	}
    	if (computer.getCategoryFilled() == CategoryType.THREES)
    	{
    		myView.setComputerScore(2, value);
    	}
    	if (computer.getCategoryFilled() == CategoryType.FOURS)
    	{
    		myView.setComputerScore(3, value);
    	}
    	if (computer.getCategoryFilled() == CategoryType.FIVES)
    	{
    		myView.setComputerScore(4, value);
    	}
    	if (computer.getCategoryFilled() == CategoryType.SIXES)
    	{
    		myView.setComputerScore(5, value);
    	}
    	if (computer.getCategoryFilled() == CategoryType.THREE_OF_KIND)
    	{
    		myView.setComputerScore(6, value);
    	}
    	if (computer.getCategoryFilled() == CategoryType.FOUR_OF_KIND)
    	{
    		myView.setComputerScore(7, value);
    	}
    	if (computer.getCategoryFilled() == CategoryType.FULL_HOUSE)
    	{
    		myView.setComputerScore(8, value);
    	}
    	if (computer.getCategoryFilled() == CategoryType.SMALL_STRAIGHT)
    	{
    		myView.setComputerScore(9, value);
    	}
    	if (computer.getCategoryFilled() == CategoryType.LARGE_STRAIGHT)
    	{
    		myView.setComputerScore(10, value);
    	}
    	if (computer.getCategoryFilled() == CategoryType.YAHTZEE)
    	{
    		myView.setComputerScore(11, value);
    	}
    	if (computer.getCategoryFilled() == CategoryType.CHANCE)
    	{
    		myView.setComputerScore(12, value);
    	}
    	
    	fillTotals();
    }
    
    /**
     * Fills in the totals after enough categories were filled.
     * Erases text at categories that are not filled.
     * Displays message at the end of the game telling you if you won or lost.
     */
    public void fillTotals()
    {
    	Vector<Player> players = myModel.getPlayers();
    	Player player = players.get(0);
    	Player computer = players.get(1);
    	ScoreCard playerScorecard = player.getScoreCard();
    	ScoreCard computerScorecard = ( (ComputerPlayer) computer).sc();
    	Vector<Category> lowerCategories = playerScorecard.getMyLowerCategories();
    	Vector<Category> upperCategories = playerScorecard.getMyUpperCategories();
    	Vector<Category> cpLowerCategories = computerScorecard.getMyLowerCategories();
    	Vector<Category> cpUpperCategories = computerScorecard.getMyUpperCategories();
    	int computerGrand = computerScorecard.getGrandTotal();
    	int playerGrand = playerScorecard.getGrandTotal();
    	
    	myPlayerFilledAllUpper = true;
    	for(int i = 0; i < 6; i++)
    	{
    		if (upperCategories.get(i) == null)
    		{
    			myView.erasePossibleScores(i);
    			myPlayerFilledAllUpper = false;
    		}
    	}
    	if (myPlayerFilledAllUpper == true)
    	{
    		myView.setPlayerTotals(0,playerScorecard.getUpperScore());
    		myView.setPlayerTotals(1,playerScorecard.getUpperTotal());
    	}
    	
    	myCpFilledAllUpper = true;
    	for(int i = 0; i < 6; i++)
    	{
    		if (cpUpperCategories.get(i) == null)
    		{
    			myCpFilledAllUpper = false;
    		}
    	}
    	if (myCpFilledAllUpper == true)
    	{
    		myView.setComputerTotals(0,computerScorecard.getUpperScore());
    		myView.setComputerTotals(1,computerScorecard.getUpperTotal());
    	}
	
    	myPlayerFilledAllLower = true;
    	for(int i = 0; i < 7; i++)
    	{
    		if (lowerCategories.get(i) == null)
    		{
    			myView.erasePossibleScores(i+6);
    			myPlayerFilledAllLower = false;
    		}
    	}
  	
    	myCpFilledAllLower = true;
    	for(int i = 0; i < 7; i++)
    	{
    		if (cpLowerCategories.get(i) == null)
    		{
    			myCpFilledAllLower = false;
    		}
    	}
    	if (myCpFilledAllLower == true)
    	{
    		myView.setComputerTotals(2,computerScorecard.getLowerTotal());
    	}

    	if (myPlayerFilledAllLower == true)
    	{
    		myView.setPlayerTotals(2,playerScorecard.getLowerTotal());
    	}
    	
    	if (myCpFilledAllUpper == true && myCpFilledAllLower == true)
    	{
    		myView.setComputerTotals(3,computerScorecard.getGrandTotal());
    	}

    	if (myPlayerFilledAllUpper == true && myPlayerFilledAllLower == true)
    	{
    		myView.setPlayerTotals(3,playerScorecard.getGrandTotal());
    	}
    	
    	if (myPlayerFilledAllLower == true && myPlayerFilledAllUpper == true
    			&& myCpFilledAllLower == true && myCpFilledAllUpper == true)
    	{
    		myModel.finishGame();
    		if (computerGrand > playerGrand)
    		{
	    		myView.setMessage("<html><body><center>The computer won.<br>"
	    				+ "Better luck next time!</center></body></html>");
    		}
    		if (computerGrand < playerGrand)
    		{
	    		myView.setMessage("<html><body><center>Congratulations.<br>"
	    				+ "You won!</center></body></html>");
    		}
    		if (computerGrand == playerGrand)
    		{
	    		myView.setMessage("<html><body><center>You tied with the computer.<br>"
	    				+ "Everyone wins!</center></body></html>");
    		}
    	}
    }
    
    
    /**
     * Set all dice to be rolled for the player's next turn
     */
    public void setAllDiceToRoll()
    {
    	Roller roller = myModel.getRoller();
    	roller.setToRoll(new boolean[] {true,true,true,true,true});
		for (int i = 0; i < 5; i++)
    	{
    		myView.selectToRoll(i);
    	}
    }
    
    
    /**
     * Method for the category buttons in the view.
     * fills the category that was pressed by the player.
     * 
     * @param index The index of the category button pressed
     */
    public void fillCategory(Integer index)
    {
    	Player playerUp = myModel.getPlayerUp();
    	if (!playerUp.getName().equals("Computer"))
    	{
	    	Vector<Player> players = myModel.getPlayers();
	    	Player player = players.get(0);
	    	ScoreCard playerScorecard = player.getScoreCard();
	    	Roller roller = myModel.getRoller();
	    	int[] values = roller.getDiceValues();
	    	Vector<Category> lowerCategories = playerScorecard.getMyLowerCategories();
	    	Vector<Category> upperCategories = playerScorecard.getMyUpperCategories();
	    	
	    	if (myModel.getNumberRollsUsed() != 0 )
	    	{
		    	if (index == 0 && upperCategories.get(0) == null)
		    	{
		    		playerScorecard.fillCategory(CategoryType.ONES, values);
		    		int value = playerScorecard.getCategory(CategoryType.ONES).getValue();
		    		myView.setPlayerScore(index, value);
		    		setAllDiceToRoll();
			    	myModel.switchPlayerUp();
			    	computerTurn();
		    	}
		    	else if (index == 1 && upperCategories.get(1) == null)
		    	{
		    		playerScorecard.fillCategory(CategoryType.TWOS, values);
		    		int value = playerScorecard.getCategory(CategoryType.TWOS).getValue();
		    		myView.setPlayerScore(index, value);
		    		setAllDiceToRoll();
			    	myModel.switchPlayerUp();
			    	computerTurn();
		    	}
		    	else if (index == 2 && upperCategories.get(2) == null)
		    	{
		    		playerScorecard.fillCategory(CategoryType.THREES, values);
		    		int value = playerScorecard.getCategory(CategoryType.THREES).getValue();
		    		myView.setPlayerScore(index, value);
		    		setAllDiceToRoll();
			    	myModel.switchPlayerUp();
			    	computerTurn();
		    	}
		    	else if (index == 3 && upperCategories.get(3) == null)
		    	{
		    		playerScorecard.fillCategory(CategoryType.FOURS, values);
		    		int value = playerScorecard.getCategory(CategoryType.FOURS).getValue();
		    		myView.setPlayerScore(index, value);
		    		setAllDiceToRoll();
			    	myModel.switchPlayerUp();
			    	computerTurn();
		    	}
		    	else if (index == 4 && upperCategories.get(4) == null)
		    	{
		    		playerScorecard.fillCategory(CategoryType.FIVES, values);
		    		int value = playerScorecard.getCategory(CategoryType.FIVES).getValue();
		    		myView.setPlayerScore(index, value);
		    		setAllDiceToRoll();
			    	myModel.switchPlayerUp();
			    	computerTurn();
		    	}
		    	else if (index == 5 && upperCategories.get(5) == null)
		    	{
		    		playerScorecard.fillCategory(CategoryType.SIXES, values);
		    		int value = playerScorecard.getCategory(CategoryType.SIXES).getValue();
		    		myView.setPlayerScore(index, value);
		    		setAllDiceToRoll();
			    	myModel.switchPlayerUp();
			    	computerTurn();
		    	}
		    	else if (index == 6 && lowerCategories.get(0) == null)
		    	{
		    		playerScorecard.fillCategory(CategoryType.THREE_OF_KIND, values);
		    		int value = playerScorecard.getCategory(CategoryType.THREE_OF_KIND).getValue();
		    		myView.setPlayerScore(index, value);
		    		setAllDiceToRoll();
			    	myModel.switchPlayerUp();
			    	computerTurn();
		    	}
		    	else if (index == 7 && lowerCategories.get(1) == null)
		    	{
		    		playerScorecard.fillCategory(CategoryType.FOUR_OF_KIND, values);
		    		int value = playerScorecard.getCategory(CategoryType.FOUR_OF_KIND).getValue();
		    		myView.setPlayerScore(index, value);
		    		setAllDiceToRoll();
			    	myModel.switchPlayerUp();
			    	computerTurn();
		    	}
		    	else if (index == 8 && lowerCategories.get(2) == null)
		    	{
		    		playerScorecard.fillCategory(CategoryType.FULL_HOUSE, values);
		    		int value = playerScorecard.getCategory(CategoryType.FULL_HOUSE).getValue();
		    		myView.setPlayerScore(index, value);
		    		setAllDiceToRoll();
			    	myModel.switchPlayerUp();
			    	computerTurn();
		    	}
		    	else if (index == 9 && lowerCategories.get(3) == null)
		    	{
		    		playerScorecard.fillCategory(CategoryType.SMALL_STRAIGHT, values);
		    		int value = playerScorecard.getCategory(CategoryType.SMALL_STRAIGHT).getValue();
		    		myView.setPlayerScore(index, value);
		    		setAllDiceToRoll();
			    	myModel.switchPlayerUp();
			    	computerTurn();
		    	}
		    	else if (index == 10 && lowerCategories.get(4) == null)
		    	{
		    		playerScorecard.fillCategory(CategoryType.LARGE_STRAIGHT, values);
		    		int value = playerScorecard.getCategory(CategoryType.LARGE_STRAIGHT).getValue();
		    		myView.setPlayerScore(index, value);
		    		setAllDiceToRoll();
			    	myModel.switchPlayerUp();
			    	computerTurn();
		    	}
		    	else if (index == 11 && lowerCategories.get(5) == null)
		    	{
		    		playerScorecard.fillCategory(CategoryType.YAHTZEE, values);
		    		int value = playerScorecard.getCategory(CategoryType.YAHTZEE).getValue();
		    		myView.setPlayerScore(index, value);
		    		setAllDiceToRoll();
			    	myModel.switchPlayerUp();
			    	computerTurn();
		    	}
		    	else if (index == 12 && lowerCategories.get(6) == null)
		    	{
		    		playerScorecard.fillCategory(CategoryType.CHANCE, values);
		    		int value = playerScorecard.getCategory(CategoryType.CHANCE).getValue();
		    		myView.setPlayerScore(index, value);
		    		setAllDiceToRoll();
			    	myModel.switchPlayerUp();
			    	computerTurn();
		    	}
		    	fillTotals();
	    	}
    	}
    }
}
