package yahtzee.controller;

/**
 * Main method that runs the Yahtzee game.
 * 
 * @author Michael Chen
 * @author Mikhail Maksimov
 */
public class YahtzeeMain {

	public static void main(String[] args)
	{
		new Controller();
	}
}
