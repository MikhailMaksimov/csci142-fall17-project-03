package yahtzee.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.lang.reflect.Method;
import java.util.Enumeration;

import javax.swing.*;
import yahtzee.controller.Controller;

@SuppressWarnings("serial")
/**
 * View class for the Yahtzee game.
 * Displays the GUI.
 * 
 * @author Michael Chen
 * @author Mikhail Maksimov
 */
public class View extends JFrame {
	
	public static final int XDIE = 288; //x position of first die
	public static final int YDIE = 400; //y position of all dice
	public static final int DIE_GAP = 70; //distance between the top right corner of each die
	
	private Controller myController;
	
	private ImageIcon myYahtzeeLogo;
	private ImageIcon myTableImg;
	private ImageIcon myMessageFrameImg;
	private ImageIcon myButtonImg;
	private ImageIcon[] myDieFaceImages;
	
	private Font font;
	
	private JPanel myTablePanel;
	private JPanel myScorePanel;
	
	private JLabel myMessage;
	private JLabel myTable;
	private JLabel myMessageFrame;
	private JLabel myPlayerName;
	private JLabel myComputerName;
	private JLabel myBlank;
	private JLabel myUpperSection;
	private JLabel myLowerSection;
	private JLabel[] myCategories;
	private JLabel[] myComputerScores;
	private JLabel[] myCategoryTotals;
	private JLabel[] myComputerTotals;
	private JLabel[] myPlayerTotals;
	
	private JButton myRollDiceButton;
	private JButton[] myDieButtons;
	private JButton[] myScoreButtons;
	
	private ButtonListener myRollDiceListener;
	private ButtonListener[] mySelectDiceListener;
	private ButtonListener[] myScoreListener;
	
	private int myDieLength;
	
	/**
	 * Constructor for the view used to layout the GUI.
	 * 
	 * @param controller The controller object that instantiated this view
	 */
	public View(Controller controller)
	{
		myController = controller;
		
		font = new Font("Serif", Font.BOLD, 14);
		setUIFont (new javax.swing.plaf.FontUIResource("Serif",Font.BOLD,12));
		
		myYahtzeeLogo = new ImageIcon("images/yahtzee_logo.png");
		myTableImg = new ImageIcon("images/table.jpg");
		myMessageFrameImg = new ImageIcon("images/message_frame.png");
		myButtonImg = new ImageIcon("images/button.png");
		myDieFaceImages = new ImageIcon[6];
		
		myTablePanel = new JPanel();
		myScorePanel = new JPanel();
		
		myMessage = new JLabel("Press 'ROLL DICE' to begin.",SwingConstants.CENTER);
		myTable = new JLabel(myTableImg);
		myMessageFrame = new JLabel(myMessageFrameImg);
		myPlayerName = new JLabel("",SwingConstants.CENTER);
		myComputerName = new JLabel("Computer",SwingConstants.CENTER);
		myBlank = new JLabel();
		myUpperSection = new JLabel("Upper Section",SwingConstants.CENTER);
		myLowerSection = new JLabel("Lower Section",SwingConstants.CENTER);
		myCategories = new JLabel[13];
		myComputerScores = new JLabel[13];
		myCategoryTotals = new JLabel[4];
		myComputerTotals = new JLabel[4];
		myPlayerTotals = new JLabel[4];
		
		myRollDiceButton = new JButton(myButtonImg);
		myDieButtons = new JButton[5];
		myScoreButtons = new JButton[13];
		
		mySelectDiceListener = new ButtonListener[5];
		myScoreListener = new ButtonListener[13];
		
		this.associateListeners();

		setUpDice();
		setUpScorecard();
		
		myTable.setBounds(0,0,1280, 720);
		
		int frameLength = myMessageFrameImg.getIconWidth();
		int frameHeight = myMessageFrameImg.getIconHeight();
		myMessageFrame.setBounds((460-(frameLength/2)), 152, frameLength, frameHeight);
		
		int messageLength = 450;
		myMessage.setBounds((460-(messageLength/2)),200,messageLength,57);
		myMessage.setFont(font);
		
		int buttonLength = myButtonImg.getIconWidth();
		int buttonHeight = myButtonImg.getIconHeight();
		myRollDiceButton.setBorderPainted(false);
		myRollDiceButton.setContentAreaFilled(false);
		myRollDiceButton.setBounds((460-(buttonLength/2)), 493, buttonLength, buttonHeight);
		myRollDiceButton.addMouseListener(myRollDiceListener);
		
		myTablePanel.setLayout(null);
		myTablePanel.setPreferredSize(new Dimension(1280, 720));
		myTablePanel.add(myScorePanel);
		myTablePanel.add(myMessage);
		myTablePanel.add(myMessageFrame);
		myTablePanel.add(myRollDiceButton);
		myTablePanel.add(myTable);
		
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setIconImage(myYahtzeeLogo.getImage());
		this.setTitle("Yahtzee");
		this.add(myTablePanel);
		this.pack();
		this.setLocationRelativeTo(null);		
		this.associateListeners();	
	}
	
	/**
	 * Sets the default font for the entire game.
	 * 
	 * @param f The font I want to use
	 */
	public void setUIFont (javax.swing.plaf.FontUIResource f)
	{
		Enumeration<Object> keys = UIManager.getDefaults().keys();
		while (keys.hasMoreElements())
		{
			Object key = keys.nextElement();
			Object value = UIManager.get (key);
			if (value instanceof javax.swing.plaf.FontUIResource)
			{
				UIManager.put (key, f);
			}
	    }
	}
	
	
	/**
	 * Initializes dice buttons and images
	 */
	public void setUpDice()
	{
		for (int i = 0; i < 6; i++)
		{
			myDieFaceImages[i] = new ImageIcon("images/" + (i+1) + ".png");
		}
		for (int i = 0; i < 5; i++)
		{
			myDieLength = myDieFaceImages[0].getIconWidth();
			myDieButtons[i] = new JButton(myDieFaceImages[i]);
			myDieButtons[i].setBounds(XDIE+(DIE_GAP*i), YDIE, myDieLength, myDieLength);
			myDieButtons[i].setBorderPainted(false);
			myDieButtons[i].setContentAreaFilled(false);
			myDieButtons[i].addMouseListener(mySelectDiceListener[i]);
			myTablePanel.add(myDieButtons[i]);
		}
	}
	
	
	/**
	 * Initializes all the labels and buttons in the score panel
	 */
	public void setUpScorecard()
	{
		myScorePanel.setBounds(920, 70, 285, 582);
		myScorePanel.setBackground(Color.BLACK);
		GridBagLayout gridbag = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		gridbag.columnWidths = new int[] {119,82,82};
		int rh = 29; //row height
		gridbag.rowHeights = new int[] 
				{rh,rh,rh,rh,rh,rh,rh,rh,rh,rh,rh,rh,rh,rh,rh,rh,rh,rh,rh,rh};
		myScorePanel.setLayout(gridbag);
		
		c.gridx = GridBagConstraints.RELATIVE;
		c.gridy = 0;
		c.insets = new Insets(1,1,1,1);
		c.fill = GridBagConstraints.BOTH;
		
		myBlank.setOpaque(true);
		myBlank.setBackground(Color.WHITE);
		myScorePanel.add(myBlank,c);
		
		myPlayerName.setOpaque(true);
		myPlayerName.setBackground(Color.WHITE);
		myScorePanel.add(myPlayerName,c);
		
		myComputerName.setOpaque(true);
		myComputerName.setBackground(Color.WHITE);
		myScorePanel.add(myComputerName,c);
		c.gridy++;
		
		c.gridwidth = 3;
		myUpperSection.setOpaque(true);
		myUpperSection.setBackground(Color.WHITE);
		myScorePanel.add(myUpperSection,c);
		c.gridwidth = 1;
		
		for (int i = 0; i < 6; i++)
		{
			c.gridy++;
			myCategories[i] = new JLabel();
			myCategories[i].setOpaque(true);
			myCategories[i].setBackground(Color.WHITE);
			myScorePanel.add(myCategories[i],c);
			
			myScoreButtons[i] = new JButton();
			myScoreButtons[i].setOpaque(true);
			myScoreButtons[i].setBorderPainted(false);
			myScoreButtons[i].setBackground(Color.WHITE);
			myScoreButtons[i].addMouseListener(myScoreListener[i]);
			myScorePanel.add(myScoreButtons[i],c);
			
			myComputerScores[i] = new JLabel("", SwingConstants.CENTER);
			myComputerScores[i].setOpaque(true);
			myComputerScores[i].setBackground(Color.WHITE);
			myScorePanel.add(myComputerScores[i],c);
		}
		
		for (int i = 0; i < 2; i++)
		{
			c.gridy++;
			myCategoryTotals[i] = new JLabel();
			myCategoryTotals[i].setOpaque(true);
			myCategoryTotals[i].setBackground(Color.WHITE);
			myScorePanel.add(myCategoryTotals[i],c);
			
			myPlayerTotals[i] = new JLabel("", SwingConstants.CENTER);
			myPlayerTotals[i].setOpaque(true);
			myPlayerTotals[i].setBackground(Color.WHITE);
			myScorePanel.add(myPlayerTotals[i],c);
			
			myComputerTotals[i] = new JLabel("", SwingConstants.CENTER);
			myComputerTotals[i].setOpaque(true);
			myComputerTotals[i].setBackground(Color.WHITE);
			myScorePanel.add(myComputerTotals[i],c);	
		}
		
		c.gridy++;
		c.gridwidth = 3;
		myLowerSection.setOpaque(true);
		myLowerSection.setBackground(Color.WHITE);
		myScorePanel.add(myLowerSection,c);
		c.gridwidth = 1;
		
		for (int i = 6; i < 13; i++)
		{
			c.gridy++;
			myCategories[i] = new JLabel();
			myCategories[i].setOpaque(true);
			myCategories[i].setBackground(Color.WHITE);
			myScorePanel.add(myCategories[i],c); 
			
			myScoreButtons[i] = new JButton();
			myScoreButtons[i].setOpaque(true);
			myScoreButtons[i].setBorderPainted(false);
			myScoreButtons[i].setBackground(Color.WHITE);
			myScoreButtons[i].addMouseListener(myScoreListener[i]);
			myScorePanel.add(myScoreButtons[i],c);
			
			myComputerScores[i] = new JLabel(" ", SwingConstants.CENTER);
			myComputerScores[i].setOpaque(true);
			myComputerScores[i].setBackground(Color.WHITE);
			myScorePanel.add(myComputerScores[i],c);
		}
		
		for (int i = 2; i < 4; i++)
		{
			c.gridy++;
			myCategoryTotals[i] = new JLabel();
			myCategoryTotals[i].setOpaque(true);
			myCategoryTotals[i].setBackground(Color.WHITE);
			myScorePanel.add(myCategoryTotals[i],c);
			
			myPlayerTotals[i] = new JLabel("", SwingConstants.CENTER);
			myPlayerTotals[i].setOpaque(true);
			myPlayerTotals[i].setBackground(Color.WHITE);
			myScorePanel.add(myPlayerTotals[i],c);
			
			myComputerTotals[i] = new JLabel("", SwingConstants.CENTER);
			myComputerTotals[i].setOpaque(true);
			myComputerTotals[i].setBackground(Color.WHITE);
			myScorePanel.add(myComputerTotals[i],c);	
		}
		
		myCategories[0].setText("   Ones");
		myCategories[1].setText("   Twos");
		myCategories[2].setText("   Threes");
		myCategories[3].setText("   Fours");
		myCategories[4].setText("   Fives");
		myCategories[5].setText("   Sixes");
		myCategoryTotals[0].setText("   Upper Score");
		myCategoryTotals[1].setText("   Upper Total");
		myCategories[6].setText("   Three of a Kind");
		myCategories[7].setText("   Four of a Kind");
		myCategories[8].setText("   Full House");
		myCategories[9].setText("   Small Straight");
		myCategories[10].setText("   Large Straight");
		myCategories[11].setText("   YAHTZEE");
		myCategories[12].setText("   Chance");
		myCategoryTotals[2].setText("   Lower Total");
		myCategoryTotals[3].setText("   Grand Total");
	}
	
	
	/**
     * Associates each component's listener with the controller
     * and the correct method to invoke when triggered.
     *
     * <pre>
     * pre:  the controller class has be instantiated
     * post: all listeners have been associated to the controller
     *       and the method it must invoke
     * </pre>
     */
	public void associateListeners()
    {
        Class<? extends Controller> controllerClass;
        Method rollDiceMethod, selectDiceMethod, fillCategoryMethod;
        Class<?>[] classArgs;
        Integer[] args;
        
        controllerClass = myController.getClass();
        rollDiceMethod = null;
        selectDiceMethod = null;  
        fillCategoryMethod = null;
        classArgs = new Class[1];
        args = new Integer[1];
        
        
        // Set argument types for method invokations
        try
        {
        	classArgs[0] = Class.forName("java.lang.Integer");
        }
        catch (ClassNotFoundException e)
        {
        	String error = e.toString();
        	System.out.println(error);
        }
        
        // Associate method names with actual methods to be invoked
        try
        {
        	 rollDiceMethod = controllerClass.getMethod("rollDice",(Class<?>[])null);
        	 selectDiceMethod = controllerClass.getMethod("selectDice",classArgs);
        	 fillCategoryMethod = controllerClass.getMethod("fillCategory",classArgs);
        }
        catch(NoSuchMethodException exception)
        {
           String error = exception.toString();
           System.out.println(error);
        }
        catch(SecurityException exception)
        {
           String error = exception.toString();
           System.out.println(error);
        }
        
        // initialize button listeners
        myRollDiceListener = new ButtonListener(myController, rollDiceMethod, null);
        for (int i = 0; i < 5; i++)
        {
        	args = new Integer[1];
        	args[0] = new Integer(i);
        	mySelectDiceListener[i] = new ButtonListener(myController, selectDiceMethod, args);
        }
        for (int i = 0; i < 13; i++)
        {
        	args = new Integer[1];
        	args[0] = new Integer(i);
        	myScoreListener[i] = new ButtonListener(myController, fillCategoryMethod, args);
        }
    }
	
	
	/**
	 * Changes the image for the face value of a die.
	 * 
	 * @param dieButton The index of the die button pressed
	 * @param faceImg The new index of the face value
	 */
	public void changeImage(int dieButton, int faceImg)
	{
		myDieButtons[dieButton].setIcon(myDieFaceImages[(faceImg)]);
	}
	
	
	/**
	 * Sets a die to be kept (as opposed to rolled).
	 * 
	 * @param dieButton The index of the die button chosen
	 */
	public void selectToKeep(int dieButton)
	{
		myDieButtons[dieButton].setBounds(myDieButtons[dieButton].getX(),YDIE+160,myDieLength,myDieLength);
	}
	
	
	/**
	 * Sets a die to be rolled (as opposed to being kept).
	 * 
	 * @param dieButton The index of the die button chosen
	 */
	public void selectToRoll(int dieButton)
	{
		myDieButtons[dieButton].setBounds(myDieButtons[dieButton].getX(),YDIE,myDieLength,myDieLength);
	}
	
	
	/**
	 * Sets the name of the player on the score card.
	 * 
	 * @param name Name of the player
	 */
	public void setPlayerName(String name)
	{
		myPlayerName.setText(name);
	}
	
	
	/**
	 * Sets the message to be displayed in the fancy frame.
	 * 
	 * @param text The text that I want to show
	 */
	public void setMessage(String text)
    {
        myMessage.setText(text);
    }
	
	
	/**
	 * Sets the score for a certain category for the player.
	 * 
	 * @param button The index of the category button
	 * @param value The value that you want to put in the button
	 */
	public void setPlayerScore(int button, int value)
	{
		myScoreButtons[button].setForeground(Color.BLACK);
		myScoreButtons[button].setText(""+value);
	}
	
	
	/**
	 * Sets the one of category totals for the player.
	 * 
	 * @param label The index of the label I want to set
	 * @param value The value I want to put in the label
	 */
	public void setPlayerTotals(int label, int value)
	{
		myPlayerTotals[label].setText(""+value);
	}
	
	
	/**
	 * Sets the score for a certain category for the computer.
	 * 
	 * @param label The index of the label I want to set
	 * @param value The value I want to put in the label
	 */
	public void setComputerScore(int label, int value)
	{
		myComputerScores[label].setText(""+value);
	}
	
	
	/**
	 * Sets the one of category totals for the computer.
	 * 
	 * @param panel The index of the label I want to set
	 * @param value The value I want to put in the label
	 */
	public void setComputerTotals(int panel, int value)
	{
		myComputerTotals[panel].setText(""+value);
	}
	
	
	/**
	 * Shows possible score (colored red) in a category button.
	 * 
	 * @param button The index of the button for a category
	 * @param value The predicted value for the score
	 */
	public void showPossibleScores(int button, int value)
	{
		myScoreButtons[button].setForeground(Color.RED);
		myScoreButtons[button].setText("" + value);
	}
	
	
	/**
	 * Erases possible score from a a category button.
	 * 
	 * @param button The index of the category button
	 */
	public void erasePossibleScores(int button)
	{
		myScoreButtons[button].setText("");
	}
}
