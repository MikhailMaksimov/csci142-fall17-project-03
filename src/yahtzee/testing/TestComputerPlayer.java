package yahtzee.testing;

import static org.junit.Assert.*;

import org.junit.Test;

import yahtzee.model.CategoryType;

import yahtzee.model.ComputerPlayer;

/**
 * Tests all of the methods within the Player class to ensure
 * they run properly. These tests make certain assumptions about
 * the behavior of the methods, all of which should work if the
 * methods are implemented correctly.
 * 
 */


public class TestComputerPlayer 
{

	@Test
    public void selectCategory()
    {
		ComputerPlayer player=new ComputerPlayer("Computer");
		int[] goodnums= {1,2,3,4,5};
		assertTrue(player.selectCategoryToFill(goodnums).equals(CategoryType.LARGE_STRAIGHT));
    }
	@Test
    public void selectReroll()
    {
		ComputerPlayer player=new ComputerPlayer("Computer");
		int[] goodnums= {1,2,2,4,5};
		boolean[] check= player.selectDiceToReroll(goodnums);
		for(int i=0;i<goodnums.length;i++)
		{
			assertTrue(check[0]);
			assertFalse(check[1]);
			assertFalse(check[2]);
			assertTrue(check[3]);
			assertTrue(check[4]);
		}
    }

}